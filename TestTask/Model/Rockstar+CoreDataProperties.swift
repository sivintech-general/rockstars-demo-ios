//
//  Rockstar+CoreDataProperties.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 19.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Rockstar {

    @NSManaged var firstname: String?
    @NSManaged var lastname: String?
    @NSManaged var status: String?
    @NSManaged var hisface: String?
    @NSManaged var bookmarked: Bool

}
