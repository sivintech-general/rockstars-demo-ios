//
//  Rockstar.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 18.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import Foundation
import CoreData

class Rockstar: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func fetchRequest(forPredicate predicate: NSPredicate?, inManagedObjectContext context: NSManagedObjectContext) -> NSFetchRequest {
        
        let fetchRequest = NSFetchRequest()
        fetchRequest.entity = NSEntityDescription.entityForName("Rockstar", inManagedObjectContext: context)
        
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "lastname", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
}
