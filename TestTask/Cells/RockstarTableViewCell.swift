//
//  RockstarTableViewCell.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 18.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit

class RockstarTableViewCell: UITableViewCell {

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var statusTextLabel: UILabel!
    @IBOutlet weak var bookmarkedSwitch: UISwitch!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var switchStateHandler: ((Bool) -> Void)?
    
    @IBAction func switchDidChangeState(sender: UISwitch) {
        
        switchStateHandler?(sender.on)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        firstNameLabel.text = nil
        lastNameLabel.text = nil
        statusTextLabel.text = nil
        
        switchStateHandler = nil // Crucial thing. If we hadn't do that, there would be a big mess.
    }
    
    func fillWithRockstar(rockstar: Rockstar) {
        firstNameLabel.text = rockstar.firstname ?? ""
        lastNameLabel.text = rockstar.lastname ?? ""
        statusTextLabel.text = rockstar.status ?? ""
        bookmarkedSwitch.on = rockstar.bookmarked
        
        if let avatarFilename = rockstar.hisface
        {
            if let imageURL = RockstarsViewModel.baseURL?.URLByAppendingPathComponent(avatarFilename) {
                avatarImageView.sd_setImageWithURL(imageURL)
            }
        }

    }
}
