//
//  RockstarsViewModel.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 18.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit
import CoreData
import RestKit

/*
This is a multi-instance class that serves as the middleman between the Model 
layer (RESTKIT object manager / CoreData) and the NSFetchedResultsController layer.
*/

class RockstarsViewModel
{
    // a.k.a. API root
    static var baseURL: NSURL! {
        get {
            return (UIApplication.sharedApplication().delegate as? AppDelegate)?.objectManager.baseURL
        }
    }
    
    // RESTKit-driven method for getting contact entities (Rockstars) from the back end.
    func requestRockstars(withCompletion completion:(Bool, NSError!) -> Void) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        guard let theRESTKitObjectManager = appDelegate?.objectManager
            else {
                let error = NSError(domain: NSStringFromClass(self.dynamicType), code: 101, userInfo: [NSLocalizedDescriptionKey : "Unable to access RESTKit's object manager"])
                completion(false, error)
                return
        }
        
        let store = theRESTKitObjectManager.managedObjectStore
        
        let rockstarEntityMapping = RKEntityMapping(forEntityForName: "Rockstar", inManagedObjectStore: store)
        rockstarEntityMapping.identificationAttributes = ["firstname", "lastname", "hisface"]
        rockstarEntityMapping.addAttributeMappingsFromDictionary(["firstname" : "firstname",
            "lastname" : "lastname",
            "hisface" : "hisface",
            "status" : "status"])
        
        let responseDescriptor = RKResponseDescriptor(mapping: rockstarEntityMapping, method: RKRequestMethod.GET, pathPattern: "contacts.json", keyPath: "contacts", statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.Successful))
        
        theRESTKitObjectManager.addResponseDescriptor(responseDescriptor)
        
        theRESTKitObjectManager.getObjectsAtPath("contacts.json", parameters: nil,
        success: { (operation: RKObjectRequestOperation!, mappingResult: RKMappingResult!) -> Void in
            if (mappingResult.array() is [Rockstar]?) {
                
                appDelegate?.saveContext()
                completion(true, nil)
            }
            else {
                let error = NSError(domain: NSStringFromClass(self.dynamicType), code: 102, userInfo: [NSLocalizedDescriptionKey : "Unexpected response type. See userInfo[\"responseDescription\"] for more details.",
                    "responseDescription" : mappingResult.array().description])
                completion(false, error)
            }
            
        },
        failure: { (operation: RKObjectRequestOperation!, error: NSError!) -> Void in
                completion(false, error)
        })
    }
    
//MARK: - Wrappers
    
    // Returns an NSFetchedResultsController for bookmarks.
    // It performs fetches for you, there's no need to do it afterwards.
    func bookmarkedFetchedResultsController() throws -> NSFetchedResultsController? {
        let predicate = NSPredicate(format: "bookmarked == true")
        return try fetchedResultsController(forPredicate: predicate)
    }
    
    // Returns an NSFetchedResultsController for Rockstars that contains Rockstars
    // with at least one entry of the searchQueryString within firstname and/or
    // lastname. The search is case-insensitive diacritic character-insensitive.
    // It performs fetches for you, there's no need to do it afterwards.
    func rockstarsFetchedResultsController(forSearchQueryString string: String?) throws -> NSFetchedResultsController? {
        
        var predicate: NSPredicate?
        if (string?.characters.count > 0) {
            predicate = NSPredicate(format: "firstname contains[cd] %@ OR lastname contains[cd] %@", string!, string!)
        }
        
        return try fetchedResultsController(forPredicate: predicate)
    }
    
//MARK: - Private
    
    // Generoc method for building NSFetchedResultsControllers with a given predicate.
    func fetchedResultsController(forPredicate predicate: NSPredicate?) throws -> NSFetchedResultsController? {
        
        let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        if let context = appDelegate?.managedObjectContext {
            
            let fetchRequest = Rockstar.fetchRequest(forPredicate: predicate, inManagedObjectContext: context)
            let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            
            do {
                try fetchedResultsController.performFetch()
                return fetchedResultsController
            }
            catch {
                NSLog("An error occurred while fetching rockstars. Description: \(error)")
                throw error
            }
        }
        
        return nil
    }
}
