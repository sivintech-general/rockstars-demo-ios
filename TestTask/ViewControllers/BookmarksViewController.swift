//
//  BookmarksViewController.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 18.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit
import CoreData

class BookmarksViewController: UIViewController, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    private lazy var viewModel: RockstarsViewModel = RockstarsViewModel()
    private var fetchedResultsController: NSFetchedResultsController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        viewModel.requestRockstars(withCompletion: { [unowned self] (success: Bool, error: NSError!) -> Void in
            
            if (success) {
                self.refetchAndUpdateTableView()
            }
            })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // Hardcoded as it's used once in the view controller. In theory, this 
        // number may be different to RockstarsViewController's cell height.
        return 120.0
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RockstarCell") as! RockstarTableViewCell
        
        if let rockstar = fetchedResultsController?.objectAtIndexPath(indexPath) as? Rockstar {
            // Same approarch as for RockstarsViewController.
            // In theory, it may differ from the similar one on RockstarsViewController.
            cell.fillWithRockstar(rockstar)
            cell.switchStateHandler = { (state: Bool) in
                rockstar.bookmarked = state
                (UIApplication.sharedApplication().delegate as? AppDelegate)?.saveContext()
            }
        }
        
        return cell
    }

    
    //MARK: - NSFetchedResultsControllerDelegate
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
    
    //MARK: - Private helpers
    
    
    private func refetchAndUpdateTableView() {
        // Same approarch as for RockstarsViewController.
        // In theory, the method may differ from the similar one on RockstarsViewController.
        if let resultsController = try? viewModel.bookmarkedFetchedResultsController() {
            fetchedResultsController = resultsController
            fetchedResultsController?.delegate = self
            self.tableView.reloadData()
        }
    }
}

