//
//  SplashViewController.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 19.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit

/*
This view controller acts as an example of dynamic splash screen.
*/

class SplashViewController: UIViewController {
    
    @IBOutlet weak var counterLabel: UILabel!
    
    private var fireCount: Int = 5
    private var timer: NSTimer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        timer = NSTimer(timeInterval: 1.0, target: self, selector: Selector("timerDidTick:"), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSDefaultRunLoopMode)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
//MARK: - Timer tick
    
    func timerDidTick(sender: NSTimer) {
        fireCount--
        if (fireCount == 0){
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            counterLabel.text = "\(fireCount)"
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
