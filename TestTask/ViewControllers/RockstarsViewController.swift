//
//  RockstarsViewController.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 18.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

private var splashScreenOnceToken: dispatch_once_t = 0

class RockstarsViewController: UIViewController, UISearchResultsUpdating, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITableViewDelegate {


    @IBOutlet weak var customNavigationItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: Selector("refreshControlDidChangeState:"), forControlEvents: UIControlEvents.ValueChanged)
        return control
    }()
    
    private lazy var viewModel: RockstarsViewModel = RockstarsViewModel()
    
    private lazy var searchController: UISearchController = {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = false
        searchController.hidesNavigationBarDuringPresentation = false
        
        return searchController
    }()
    
    private var fetchedResultsController: NSFetchedResultsController?

//MARK: - Lifecycle
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        // Entry point for the animated splash screen. Performed once according to
        // the token at the top scope level.
        dispatch_once(&splashScreenOnceToken, { () -> Void in
            if let splash = self.storyboard?.instantiateViewControllerWithIdentifier("Splash") {
                self.presentViewController(splash, animated: false, completion: nil)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        definesPresentationContext = true
        customNavigationItem.titleView = searchController.searchBar
        
        tableView.addSubview(refreshControl)
        
        // Initial fetch.
        viewModel.requestRockstars(withCompletion: { [unowned self] (success: Bool, error: NSError!) -> Void in
            
            if (success) {
                self.refetchAndUpdateTableView()
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Actions
    
    // This method response to the pull-down-to-refresh action.
    func refreshControlDidChangeState(sender: UIRefreshControl) {
        
        viewModel.requestRockstars(withCompletion: { [unowned self] (success: Bool, error: NSError!) -> Void in
            if (success) {
                self.refetchAndUpdateTableView()
                sender.endRefreshing()
            }
            })
    }
    
//MARK: - Private helpers
    
    // This method changes the fetchedResultsController in order to reflect the
    // actual state of the contact list (Rockstar list). The fetchRequest is bound 
    // to the RockStarsViewController in both directions self->fetchedResultsController (strong) 
    // and fetchedResultsController.delegate->self (weak).
    // At the end of the call, the method reloads the table view.
    private func refetchAndUpdateTableView() {
        
        if let resultsController = try? viewModel.rockstarsFetchedResultsController(forSearchQueryString: self.searchController.searchBar.text)
        {
            fetchedResultsController = resultsController
            fetchedResultsController?.delegate = self
            self.tableView.reloadData()
        }
    }
    


//MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // Hardcoded as it's used only once in the controller.
        return 120.0
    }
    
//MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RockstarCell") as! RockstarTableViewCell
        
        if let rockstar = fetchedResultsController?.objectAtIndexPath(indexPath) as? Rockstar {
            // Applying the Rockstart data to a cell.
            cell.fillWithRockstar(rockstar)
            
            // Binding the bookmarked property to the cell's swith via the closure.
            // This way is much more "secretive" than the direct assignment of a Rockstar to a cell.
            cell.switchStateHandler = { (state: Bool) in
                rockstar.bookmarked = state
                (UIApplication.sharedApplication().delegate as? AppDelegate)?.saveContext()
            }
        }
        
        return cell
    }
    
//MARK: - NSFetchedResultsControllerDelegate
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
    
//MARK: - UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        refetchAndUpdateTableView()
    }

}

