//
//  ProfileViewController.swift
//  TestTask
//
//  Created by Arthur Gevorkyan on 19.02.16.
//  Copyright © 2016 Arthur Gevorkyan. All rights reserved.
//

import UIKit
import MobileCoreServices
import SDWebImage
import CoreData

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Just a computed shortcut.
    private var appDelegate: AppDelegate? {
        get {
            return UIApplication.sharedApplication().delegate as? AppDelegate
        }
    }
    
    // Fetched entity. Guaranteed to exist (one is created if none found).
    private var profile: Profile? {
        get {
            let request = NSFetchRequest(entityName: "Profile")
            request.fetchLimit = 1
            
            var profile: Profile?
            
            if let fetchedProfile = (try? appDelegate?.managedObjectContext.executeFetchRequest(request).first) as? Profile
            {
                profile = fetchedProfile
            }
            else
            {
                profile = appDelegate?.managedObjectContext.insertNewObjectForEntityForName("Profile") as? Profile
            }
            return profile
        }
    }
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    // Configures and shows a UIImagePickerController on demand. Assigns self as its delegate.
    @IBAction func takePhotoButtonDidTap(sender: UIButton) {
        
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.Camera
        picker.mediaTypes = [String(kUTTypeImage)]
        picker.delegate = self
        picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
        presentViewController(picker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullNameTextField.text = profile?.fullname
        // Initial fetch from the image cache.
        SDImageCache.sharedImageCache().queryDiskCacheForKey(profile?.imagecachekey, done: { [unowned self] (image: UIImage!, cacheType: SDImageCacheType) -> Void in
            
            self.avatarImageView.image = image
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Preserving the state in case of app termination and/or view controller dismissal.
        profile?.fullname = fullNameTextField.text
        appDelegate?.saveContext()
    }
    
//MARK: - UITextFieldDelegate
    
    // Saving on editing end.
    func textFieldDidEndEditing(textField: UITextField) {
        
        profile?.fullname = textField.text
        appDelegate?.saveContext()
    }
    
    // Hiding the keyboard on the Return button tap.
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
//MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        // Retrieving the image from picker and caching it for the future.
        let image = info["UIImagePickerControllerOriginalImage"] as? UIImage
        avatarImageView.image = image
        
        let cache = SDImageCache.sharedImageCache()
        
        let imageCacheKey = "UIImagePickerControllerOriginalImage"
        
        profile?.imagecachekey = imageCacheKey
        
        // Erasing the previous image.
        cache.removeImageForKey(imageCacheKey, fromDisk: true)
        // Caching the new one.
        cache.storeImage(image, forKey: imageCacheKey)
        
        // Dismissing the picker and saving the pick.
        dismissViewControllerAnimated(true, completion: nil)
        appDelegate?.saveContext()
    }

}
