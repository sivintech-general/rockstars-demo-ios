This is a test project. Its name is Rockstars. You will have to run "pod_install" in the root folder of the project in order to be able to build and run it (see cocoapods.org for more details).
- Minimum platform version is 8.0
- Language is Swift
- There are references to RESTKit and SDWebImage in the project

P.S. Please feel free to ask questions via email: arthur.gevorkyan@sivintech.com